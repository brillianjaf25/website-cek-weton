<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Cek Weton</title>
  <link rel="stylesheet" href="style-new.css" />
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"
    integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA=="
    crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://kit.fontawesome.com/676d1da154.js" crossorigin="anonymous"></script>
</head>

<body>
  <!-- Header & Navbar -->
  <nav class="nav">
    <a href="#" class="logo" style="margin-top: 10px; margin-bottom: 10px">Weton Jawa</a>
    <i class="menu-bar fas fa-solid fa-bars"></i>

    <ul class="menu show">
      <li><a href="#">Home</a></li>
      <li><a href="Tabel Neptu.php">Tabel Neptu</a></li>
      <li><a href="Konversi.php">Konversi</a></li>
      <li><a href="Arti Neptu.php">Arti Neptu</a></li>
    </ul>
  </nav>

  <!-- MAIN -->
  <main class="main">
    <section class="main-content">
      <p class="aksara">꧋ꦮꦼꦠꦺꦴꦤ꧀ꦗꦮ</p>
      <br />
    </section>
    <h class="aksara-isi"> <b>Apa itu Neptu Jawa, Weton dan Primbon</b></h>
    <br />
    <img src="buku primbon.jpg" alt="primbon" />
    <br />

    <p class="pengertian" style="text-align: left">
      Neptu Jawa erat kaitannya dengan weton serta primbon, sebab ketiganya
      saling berhubungan untuk membaca suatu hal sesuai dengan kepentingan
      seseorang.
    </p>
    <br />

    <p class="pengertian" style="text-align: left">
      Neptu jawa adalah nilai tertentu dari masing-masing hari dari hari Senin
      hingga Minggu atau tujuh hari dalam seminggu dan nilai dari hari pasaran
      dalam Jawa yaitu lima hari dalam satu minggu. Primbon Jawa telah
      menetapkan nilai-nilai dari neptu dan masing-masing hari yang ada dalam
      pasaran jawa. lalu, nilai yang didapatkan dari perhitungan nilai neptu
      hari dengan nilai neptu pasaran diperoleh melalui weton.
    </p>
    <br />

    <p class="pengertian" style="text-align: left">
      Secara singkatnya, neptu merupakan perhitungan antara hari lahir dengan
      pasaran yang hasilnya akan menentukan weton seseorang. Sementara itu,
      weton merupakan gabungan dari tujuh hari dalam seminggu (Senin, Selasa,
      Rabu, Kamis, Jum'at, Sabtu dan Minggu) dengan lima hari dalam pasaran
      Jawa, yaitu Legi, Pahing, Wage, Pon dan Kliwon.
    </p>
    <br />

    <a class="btn" href="#">Referensi Buku Primbon</a>
  </main>

  <script src="./script.js"></script>
</body>

</html>