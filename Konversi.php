<?php
require './PHP/konversi-neptu.php';

if (isset($_POST['jumlahkan'])) {
    // $konversi = totalNeptu();

    $input_hari = $_POST['hari'];
    $input_pasaran = $_POST['pasaran'];

    $data_hari = ambilHari($input_hari);
    $data_pasaran = ambilPasaran($input_pasaran);
    $total = totalNeptu($data_hari['neptu'], $data_pasaran['neptu']);

}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Cek Weton</title>
    <link rel="stylesheet" href="style-new.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"
        integrity="sha512-aVKKRRi/Q/YV+4mjoKBsE4x3H+BkegoM/em46NNlCqNTmUYADjBbeNefNxYV7giUp0VxICtqdrbqU7iVaeZNXA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://kit.fontawesome.com/676d1da154.js" crossorigin="anonymous"></script>
</head>

<body>
    <!-- Header & Navbar -->
    <nav class="nav">
        <a href="#" class="logo" style="margin-top: 10px; margin-bottom: 10px">Weton Jawa</a>

        <i class="menu-bar fas fa-solid fa-bars"></i>

        <ul class="menu show">
            <li><a href="index.php">Home</a></li>
            <li><a href="Tabel Neptu.php">Tabel Neptu</a></li>
            <li><a href="#">Konversi</a></li>
            <li><a href="Arti Neptu.php">Arti Neptu</a></li>
        </ul>
    </nav>
    <div class="konversi-neptu">
        <h1>Form Konversi Neptu</h1>
        <form action="" method="post">
            <div class="hari">
                <label for="hari">Masukkan Hari :</label>
                <input type="text" name="hari" id="hari">
            </div>
            <div class="pasaran">
                <label for="pasaran">Masukkan Pasaran :</label>
                <input type="text" name="pasaran" id="pasaran">
            </div>
            <div class="jumlahkan">
                <button type="submit" name="jumlahkan">Jumlah</button>
            </div>
        </form>
    <?php if (isset($_POST['jumlahkan'])): ?>
        <div class="tampilkan">
            <div class="box-tampil-hari">
                <h3>Hari :</h3>
                <p class="tampil-hari"><?php echo $_POST['hari'] ?></p>
                <h3>Jumlah Neptu :</h3>
                <p class="jumlah-neptu-hari"><?php echo $data_hari['neptu'] ?></p>
            </div>
            <div class="box-tampil-pasaran">
                <h3>Pasaran :</h3>
                <p class="tampil-pasaran"><?php echo $_POST['pasaran'] ?></p>
                <h3>Jumlah Neptu :</h3>
                <p class="jumlah-neptu-pasaran"><?php echo $data_pasaran['neptu'] ?></p>
            </div>
        </div>

        <div class="box-pengertian">
            <div class="total-neptu">
                <h3>Total Neptu :</h3>
                <p><?php echo $total['neptu'] ?></p>
            </div>
            <div class="nama-neptu">
                <h3>Nama :</h3>
                <p><?php echo $total['nama'] ?></p>
            </div>
            <div class="pengertian-neptu">
                <h3>Pengertian :</h3>
                <p><?php echo $total['arti'] ?></p>
            </div>
        </div>
        <?php endif;?>
    </div>

</body>

</html>
