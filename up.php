<?php

require './PHP/koneksi.php';

global $koneksi;

for($a = 0; $a < 50; $a++){

    if ($a == 1 || $a == 9 || $a == 10 || $a == 18 || $a == 19 || $a == 27 || $a == 28 || $a == 36) {
        mysqli_query($koneksi, "INSERT INTO data_neptu(neptu, nama, arti) VALUES ('$a', 'Pegat', 'Berdasarkan dari dasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.')");
    } else if ($a == 2 || $a == 11 || $a == 20 || $a == 29) {
        mysqli_query($koneksi, "INSERT INTO data_neptu(neptu, nama, arti) VALUES ('$a', 'Ratu', 'Apabila anda memperoleh hasil perhitungan neptu weton dengan angka 2,11,20, maupun 29, maka anda cukup beruntung. karena kategori jumlah neptu weton ratu artinya adalah anda dengan pasangan anda jodoh sejati. Anda akan memiliki hubungan yang sangat harmonis, bahagia dan bahkan hubungan kalian berdua bisa membuat banyak orang iri karena kalian berdua terlihat begitu serasi.')");
    } else if ($a == 3 || $a == 12 || $a == 21 || $a == 30) {
        mysqli_query($koneksi, "INSERT INTO data_neptu(neptu, nama, arti) VALUES ('$a', 'Jodoh', 'Arti perhitungan neptu weton yang ketiga mungkin adalah arti perhitungan yang sangat diinginkan. Sebab, jika anda memiliki jumlah neptu 3, 11, 21 dan 30 maka artinya anda dan pasangan anda adalah jodoh. Lebih dari itu, anda dan pasangan anda diramalkan akan memiliki kehidupan berumah tangga yang rukun.')");
    } else if ($a == 4 || $a == 13 || $a == 22 || $a == 31) {
        mysqli_query($koneksi, "INSERT INTO data_neptu(neptu, nama, arti) VALUES ('$a', 'Topo', 'Apabila Grameds mendapatkan hasil perhitungan neptu topo, maka anda perlu meningkatkan kewaspadaan. Sebab, menurut primbon Jawa, arti neptu topo adalah bahwa anda dan pasangan akan mendapatkan suatu kesulitan ketika menjalani masa-masa awal dalam berumah tangga.')");
    } else if ($a == 5 || $a == 14 || $a == 23 || $a == 32) {
        mysqli_query($koneksi, "INSERT INTO data_neptu(neptu, nama, arti) VALUES ('$a', 'Tinari', 'Apabila anda mendapatkan hasil perhitungan 5, 14, 23, ataupun 32 maka jumlah neptu weton tersebut termasuk dalam tinari yang artinya anda tidak perlu merasa khawatir serta bisa lebih tenang. Sebab tinari artinya adalah kabar bahagia bagi anda. Kebahagiaan yang dimaksud ialah rezeki yang cukup ketika anda membangun rumah tangga. Selain itu, anda sekeluarga juga akan diberikan kemudahan untuk mencari rezeki.')");
    } else if ($a == 6 || $a == 15 || $a == 24 || $a == 33) {
        mysqli_query($koneksi, "INSERT INTO data_neptu(neptu, nama, arti) VALUES ('$a', 'Padu', 'Dalam bahasa Jawa, padu artinya adalah berdebat yang mencirikan suatu hubungan yang buruk. Sesuai dengan namanya, jumlah neptu weton padu yaitu 6, 15, 24, dan 33 mengindikasikan adanya ramalan buruk bagi kehidupan rumah tangga anda serta pasangan anda. Meskipun begitu, anda bisa lebih tenang sebab hubungan anda dan pasangan tidak akan berujung pada perceraian.')");
    } else if ($a == 7 || $a == 16 || $a == 25 || $a == 34) {
        mysqli_query($koneksi, "INSERT INTO data_neptu(neptu, nama, arti) VALUES ('$a', 'Sujanan', 'Hasil perhitungan neptu weton dalam perjodohan yang ketujuh adalah sujanan dengan jumlah angka 7, 16, 25, ataupun 34. Apabila mendapatkan hasil neptu sujanan, maka anda perlu lebih berhati-hati serta waspada. Pasalnya menurut primbon Jawa, orang yang termasuk dalam kategori sujanan adalah orang atau pasangan yang berada dalam ancaman suatu pertengkaran yang cukup besar dalam rumah tangga yang diakibatkan oleh perselingkuhan.')");
    } else if ($a == 8 || $a == 17 || $a == 26 || $a == 35) {
        mysqli_query($koneksi, "INSERT INTO data_neptu(neptu, nama, arti) VALUES ('$a', 'Pesthi', 'Menurut perhitungan neptu weton jodoh, orang-orang yang memiliki jumlah perhitungan pesthi yaitu 8, 17, 26, dan 35 adalah orang yang akan memiliki rumah tangga yang rukun dengan pasangannya. Apabila anda memiliki jumlah perhitungan neptu weton jodoh satu ini, maka anda akan memiliki kehidupan keluarga yang harmonis serta rukun.')");
    }
}


?>