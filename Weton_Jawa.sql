-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for cek_weton
CREATE DATABASE IF NOT EXISTS `cek_weton` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `cek_weton`;

-- Dumping structure for table cek_weton.cari_neptu
CREATE TABLE IF NOT EXISTS `cari_neptu` (
  `neptu` int DEFAULT NULL,
  `hari` varchar(50) DEFAULT NULL,
  `pasaran` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table cek_weton.cari_neptu: ~6 rows (approximately)
INSERT INTO `cari_neptu` (`neptu`, `hari`, `pasaran`) VALUES
	(3, 'Selasa', NULL),
	(4, 'Senin', 'Wage'),
	(5, 'Minggu', 'Legi'),
	(6, 'Jum\'at', NULL),
	(7, 'Rabu', 'Pon'),
	(8, 'Kamis', 'Kliwon'),
	(9, 'Sabtu', 'Pahing');

-- Dumping structure for table cek_weton.data_neptu
CREATE TABLE IF NOT EXISTS `data_neptu` (
  `nama` varchar(50) DEFAULT NULL,
  `neptu` int DEFAULT NULL,
  `arti` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table cek_weton.data_neptu: ~36 rows (approximately)
INSERT INTO `data_neptu` (`nama`, `neptu`, `arti`) VALUES
	('Pegat', 1, 'Berdasarkan dari hasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.'),
	('Pegat', 9, 'Berdasarkan dari hasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.'),
	('Pegat', 10, 'Berdasarkan dari hasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.'),
	('Pegat', 18, 'Berdasarkan dari hasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.'),
	('Pegat', 19, 'Berdasarkan dari hasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.'),
	('Pegat', 27, 'Berdasarkan dari hasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.'),
	('Pegat', 28, 'Berdasarkan dari hasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.'),
	('Pegat', 36, 'Berdasarkan dari hasil perhitungan neptu weton pegat, maka artinya hubungan anda termasuk kategori rawan. Pegat menurut primbon Jawa menjadi indikasi adanya kemungkinan antara anda dengan pasangan anda sering mendapatkan masalah dalam kehidupan.'),
	('Ratu', 2, 'Apabila anda memperoleh hasil perhitungan neptu weton dengan angka 2,11,20, maupun 29, maka anda cukup beruntung. karena kategori jumlah neptu weton ratu artinya adalah anda dengan pasangan anda jodoh sejati. Anda akan memiliki hubungan yang sangat harmonis, bahagia dan bahkan hubungan kalian berdua bisa membuat banyak orang iri karena kalian berdua terlihat begitu serasi.'),
	('Jodoh', 3, 'Arti perhitungan neptu weton yang ketiga mungkin adalah arti perhitungan yang sangat diinginkan. Sebab, jika anda memiliki jumlah neptu 3, 11, 21 dan 30 maka artinya anda dan pasangan anda adalah jodoh. Lebih dari itu, anda dan pasangan anda diramalkan akan memiliki kehidupan berumah tangga yang rukun.'),
	('Topo', 4, 'Apabila Grameds mendapatkan hasil perhitungan neptu topo, maka anda perlu meningkatkan kewaspadaan. Sebab, menurut primbon Jawa, arti neptu topo adalah bahwa anda dan pasangan akan mendapatkan suatu kesulitan ketika menjalani masa-masa awal dalam berumah tangga.'),
	('Tinari', 5, 'Apabila anda mendapatkan hasil perhitungan 5, 14, 23, ataupun 32 maka jumlah neptu weton tersebut termasuk dalam tinari yang artinya anda tidak perlu merasa khawatir serta bisa lebih tenang. Sebab tinari artinya adalah kabar bahagia bagi anda. Kebahagiaan yang dimaksud ialah rezeki yang cukup ketika anda membangun rumah tangga. Selain itu, anda sekeluarga juga akan diberikan kemudahan untuk mencari rezeki.'),
	('Padu', 6, 'Dalam bahasa Jawa, padu artinya adalah berdebat yang mencirikan suatu hubungan yang buruk. Sesuai dengan namanya, jumlah neptu weton padu yaitu 6, 15, 24, dan 33 mengindikasikan adanya ramalan buruk bagi kehidupan rumah tangga anda serta pasangan anda. Meskipun begitu, anda bisa lebih tenang sebab hubungan anda dan pasangan tidak akan berujung pada perceraian.'),
	('Sujanan', 7, 'Hasil perhitungan neptu weton dalam perjodohan yang ketujuh adalah sujanan dengan jumlah angka 7, 16, 25, ataupun 34. Apabila mendapatkan hasil neptu sujanan, maka anda perlu lebih berhati-hati serta waspada. Pasalnya menurut primbon Jawa, orang yang termasuk dalam kategori sujanan adalah orang atau pasangan yang berada dalam ancaman suatu pertengkaran yang cukup besar dalam rumah tangga yang diakibatkan oleh perselingkuhan.'),
	('Pesthi', 8, 'Menurut perhitungan neptu weton jodoh, orang-orang yang memiliki jumlah perhitungan pesthi yaitu 8, 17, 26, dan 35 adalah orang yang akan memiliki rumah tangga yang rukun dengan pasangannya. Apabila anda memiliki jumlah perhitungan neptu weton jodoh satu ini, maka anda akan memiliki kehidupan keluarga yang harmonis serta rukun.'),
	('Ratu', 11, 'Apabila anda memperoleh hasil perhitungan neptu weton dengan angka 2,11,20, maupun 29, maka anda cukup beruntung. karena kategori jumlah neptu weton ratu artinya adalah anda dengan pasangan anda jodoh sejati. Anda akan memiliki hubungan yang sangat harmonis, bahagia dan bahkan hubungan kalian berdua bisa membuat banyak orang iri karena kalian berdua terlihat begitu serasi.'),
	('Jodoh', 12, 'Arti perhitungan neptu weton yang ketiga mungkin adalah arti perhitungan yang sangat diinginkan. Sebab, jika anda memiliki jumlah neptu 3, 11, 21 dan 30 maka artinya anda dan pasangan anda adalah jodoh. Lebih dari itu, anda dan pasangan anda diramalkan akan memiliki kehidupan berumah tangga yang rukun.'),
	('Topo', 13, 'Apabila Grameds mendapatkan hasil perhitungan neptu topo, maka anda perlu meningkatkan kewaspadaan. Sebab, menurut primbon Jawa, arti neptu topo adalah bahwa anda dan pasangan akan mendapatkan suatu kesulitan ketika menjalani masa-masa awal dalam berumah tangga.'),
	('Tinari', 14, 'Apabila anda mendapatkan hasil perhitungan 5, 14, 23, ataupun 32 maka jumlah neptu weton tersebut termasuk dalam tinari yang artinya anda tidak perlu merasa khawatir serta bisa lebih tenang. Sebab tinari artinya adalah kabar bahagia bagi anda. Kebahagiaan yang dimaksud ialah rezeki yang cukup ketika anda membangun rumah tangga. Selain itu, anda sekeluarga juga akan diberikan kemudahan untuk mencari rezeki.'),
	('Padu', 15, 'Dalam bahasa Jawa, padu artinya adalah berdebat yang mencirikan suatu hubungan yang buruk. Sesuai dengan namanya, jumlah neptu weton padu yaitu 6, 15, 24, dan 33 mengindikasikan adanya ramalan buruk bagi kehidupan rumah tangga anda serta pasangan anda. Meskipun begitu, anda bisa lebih tenang sebab hubungan anda dan pasangan tidak akan berujung pada perceraian.'),
	('Sujanan', 16, 'Hasil perhitungan neptu weton dalam perjodohan yang ketujuh adalah sujanan dengan jumlah angka 7, 16, 25, ataupun 34. Apabila mendapatkan hasil neptu sujanan, maka anda perlu lebih berhati-hati serta waspada. Pasalnya menurut primbon Jawa, orang yang termasuk dalam kategori sujanan adalah orang atau pasangan yang berada dalam ancaman suatu pertengkaran yang cukup besar dalam rumah tangga yang diakibatkan oleh perselingkuhan.'),
	('Pesthi', 17, 'Menurut perhitungan neptu weton jodoh, orang-orang yang memiliki jumlah perhitungan pesthi yaitu 8, 17, 26, dan 35 adalah orang yang akan memiliki rumah tangga yang rukun dengan pasangannya. Apabila anda memiliki jumlah perhitungan neptu weton jodoh satu ini, maka anda akan memiliki kehidupan keluarga yang harmonis serta rukun.'),
	('Ratu', 20, 'Apabila anda memperoleh hasil perhitungan neptu weton dengan angka 2,11,20, maupun 29, maka anda cukup beruntung. karena kategori jumlah neptu weton ratu artinya adalah anda dengan pasangan anda jodoh sejati. Anda akan memiliki hubungan yang sangat harmonis, bahagia dan bahkan hubungan kalian berdua bisa membuat banyak orang iri karena kalian berdua terlihat begitu serasi.'),
	('Jodoh', 21, 'Arti perhitungan neptu weton yang ketiga mungkin adalah arti perhitungan yang sangat diinginkan. Sebab, jika anda memiliki jumlah neptu 3, 11, 21 dan 30 maka artinya anda dan pasangan anda adalah jodoh. Lebih dari itu, anda dan pasangan anda diramalkan akan memiliki kehidupan berumah tangga yang rukun.'),
	('Topo', 22, 'Apabila Grameds mendapatkan hasil perhitungan neptu topo, maka anda perlu meningkatkan kewaspadaan. Sebab, menurut primbon Jawa, arti neptu topo adalah bahwa anda dan pasangan akan mendapatkan suatu kesulitan ketika menjalani masa-masa awal dalam berumah tangga.'),
	('Tinari', 23, 'Apabila anda mendapatkan hasil perhitungan 5, 14, 23, ataupun 32 maka jumlah neptu weton tersebut termasuk dalam tinari yang artinya anda tidak perlu merasa khawatir serta bisa lebih tenang. Sebab tinari artinya adalah kabar bahagia bagi anda. Kebahagiaan yang dimaksud ialah rezeki yang cukup ketika anda membangun rumah tangga. Selain itu, anda sekeluarga juga akan diberikan kemudahan untuk mencari rezeki.'),
	('Padu', 24, 'Dalam bahasa Jawa, padu artinya adalah berdebat yang mencirikan suatu hubungan yang buruk. Sesuai dengan namanya, jumlah neptu weton padu yaitu 6, 15, 24, dan 33 mengindikasikan adanya ramalan buruk bagi kehidupan rumah tangga anda serta pasangan anda. Meskipun begitu, anda bisa lebih tenang sebab hubungan anda dan pasangan tidak akan berujung pada perceraian.'),
	('Sujanan', 25, 'Hasil perhitungan neptu weton dalam perjodohan yang ketujuh adalah sujanan dengan jumlah angka 7, 16, 25, ataupun 34. Apabila mendapatkan hasil neptu sujanan, maka anda perlu lebih berhati-hati serta waspada. Pasalnya menurut primbon Jawa, orang yang termasuk dalam kategori sujanan adalah orang atau pasangan yang berada dalam ancaman suatu pertengkaran yang cukup besar dalam rumah tangga yang diakibatkan oleh perselingkuhan.'),
	('Pesthi', 26, 'Menurut perhitungan neptu weton jodoh, orang-orang yang memiliki jumlah perhitungan pesthi yaitu 8, 17, 26, dan 35 adalah orang yang akan memiliki rumah tangga yang rukun dengan pasangannya. Apabila anda memiliki jumlah perhitungan neptu weton jodoh satu ini, maka anda akan memiliki kehidupan keluarga yang harmonis serta rukun.'),
	('Ratu', 29, 'Apabila anda memperoleh hasil perhitungan neptu weton dengan angka 2,11,20, maupun 29, maka anda cukup beruntung. karena kategori jumlah neptu weton ratu artinya adalah anda dengan pasangan anda jodoh sejati. Anda akan memiliki hubungan yang sangat harmonis, bahagia dan bahkan hubungan kalian berdua bisa membuat banyak orang iri karena kalian berdua terlihat begitu serasi.'),
	('Jodoh', 30, 'Arti perhitungan neptu weton yang ketiga mungkin adalah arti perhitungan yang sangat diinginkan. Sebab, jika anda memiliki jumlah neptu 3, 11, 21 dan 30 maka artinya anda dan pasangan anda adalah jodoh. Lebih dari itu, anda dan pasangan anda diramalkan akan memiliki kehidupan berumah tangga yang rukun.'),
	('Topo', 31, 'Apabila Grameds mendapatkan hasil perhitungan neptu topo, maka anda perlu meningkatkan kewaspadaan. Sebab, menurut primbon Jawa, arti neptu topo adalah bahwa anda dan pasangan akan mendapatkan suatu kesulitan ketika menjalani masa-masa awal dalam berumah tangga.'),
	('Tinari', 32, 'Apabila anda mendapatkan hasil perhitungan 5, 14, 23, ataupun 32 maka jumlah neptu weton tersebut termasuk dalam tinari yang artinya anda tidak perlu merasa khawatir serta bisa lebih tenang. Sebab tinari artinya adalah kabar bahagia bagi anda. Kebahagiaan yang dimaksud ialah rezeki yang cukup ketika anda membangun rumah tangga. Selain itu, anda sekeluarga juga akan diberikan kemudahan untuk mencari rezeki.'),
	('Padu', 33, 'Dalam bahasa Jawa, padu artinya adalah berdebat yang mencirikan suatu hubungan yang buruk. Sesuai dengan namanya, jumlah neptu weton padu yaitu 6, 15, 24, dan 33 mengindikasikan adanya ramalan buruk bagi kehidupan rumah tangga anda serta pasangan anda. Meskipun begitu, anda bisa lebih tenang sebab hubungan anda dan pasangan tidak akan berujung pada perceraian.'),
	('Sujanan', 34, 'Hasil perhitungan neptu weton dalam perjodohan yang ketujuh adalah sujanan dengan jumlah angka 7, 16, 25, ataupun 34. Apabila mendapatkan hasil neptu sujanan, maka anda perlu lebih berhati-hati serta waspada. Pasalnya menurut primbon Jawa, orang yang termasuk dalam kategori sujanan adalah orang atau pasangan yang berada dalam ancaman suatu pertengkaran yang cukup besar dalam rumah tangga yang diakibatkan oleh perselingkuhan.'),
	('Pesthi', 35, 'Menurut perhitungan neptu weton jodoh, orang-orang yang memiliki jumlah perhitungan pesthi yaitu 8, 17, 26, dan 35 adalah orang yang akan memiliki rumah tangga yang rukun dengan pasangannya. Apabila anda memiliki jumlah perhitungan neptu weton jodoh satu ini, maka anda akan memiliki kehidupan keluarga yang harmonis serta rukun.');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
